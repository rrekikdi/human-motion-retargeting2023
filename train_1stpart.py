import torch 
from models.skeleton_from_pointcloud import PointNetRegression
from dataset.amass_dataset import *
import torch.optim as optim
import torch.nn.functional as F
from utils.augmentation import *
import argparse
import json
from human_body_prior.tools.omni_tools import makepath

def bone_loss(sk1,sk2,parent):
    
    return (((sk1[:,1:]-sk1[:,parent][:,1:]).norm(dim=-1) / (sk2[:,1:]-sk2[:,parent][:,1:]).norm(dim=-1) )-1).mean()
   


def train_skreg(config):
    workers = config["num_workers"]
    nepoch = config["num_epochs"]
    outf = config["out_folder"]
    
    makepath(outf)
    makepath(os.path.join(outf,"model"))
    
    data_path=config["data_path"]
    batchSize = config["batch_size"]
    njoints=config["num_joints"]
   
    dataset = AmassDataset(data_path,
                    split ='train',
                    sequential=False,
                    keys=[PCD,SK],
                    fps=None)
    test_dataset = AmassDataset(data_path,
                    split ='test',
                    sequential=False,
                    keys=[PCD,SK],
                    fps=None)

    print("Training data for part 1 : %d frames"%len(dataset))
    print("Testing data for part 1 : %d frames"%len(test_dataset))
    
    dataloader = torch.utils.data.DataLoader(
        dataset,
        batch_size=batchSize,
        shuffle=True,
        num_workers= workers)

    testdataloader = torch.utils.data.DataLoader(
        test_dataset,
        batch_size=batchSize,
        shuffle=True,
        num_workers=workers )

    regressor = PointNetRegression(output_size=3, k=njoints)
    
    if config["initialization"] != "":
        regressor.load_state_dict(torch.load(config["initialization"]))
    
    optimizer = optim.Adam(regressor.parameters(), lr=0.0001, betas=(0.9, 0.999))
    scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=20, gamma=0.5)
    regressor.cuda()

    num_batch = len(dataset) / batchSize
    
    parents = [-1,   0,   0,   0,   1,   2,
    3,   4,   5,   6,   7,   8,
    9,   9,   9,  12,  13,  14,
        16,  17,  18,  19]
    df = open(os.path.join(outf,'losses1.txt'),'w')
    for epoch in range(nepoch):
        s=0
        stest=0
        for i, data in enumerate(dataloader, 0):
            
            points, target = data 
            target = target[:,:njoints]
            points, target = points.cuda(), target.cuda()
            
            centroids = points.mean(dim=-2,keepdim=True)
            points = points - centroids
            target = target - centroids
            
            if config["data_augmentation"]["noise"]:
                points =  points + torch.normal(torch.zeros_like(points),torch.ones_like(points)*0.003)
                
            if config["data_augmentation"]["rotation"]:
                R,x2,x3 = generate_random_z_axis_rotation(points.shape[0])
                
                mean_coord = torch.mean(points, dim=1,keepdim=True)
                points = apply_random_rotation(points,R.cuda(),x2.cuda(),x3.cuda(),mean_coord)
                target = apply_random_rotation(target,R.cuda(),x2.cuda(),x3.cuda(),mean_coord)
                
            if config["data_augmentation"]["norm"]:
                scale = points.norm(dim=-1).max(dim=-1)[0]
                assert((scale==0).any()==False)
                points = points/scale[:,None,None]
                target = target/scale[:,None,None]
            else:
                scale=1
                
            points = points.transpose(2, 1)
            
            optimizer.zero_grad()
            regressor = regressor.train()
            pred, _, _ = regressor(points)
            #pred = pred*scale[:,None,None]
           
            loss = F.mse_loss(pred, target) + config["bone_weight"]*bone_loss(pred,target,parents)
            print('[%d] train loss: %f' % (epoch, s/(i+1)),end='\r')
            loss.backward()
            optimizer.step()
            s=s+loss.item()
            
        s= s/num_batch
        print('[%d] train loss: %f' % (epoch, s)+'\n')
        df.write('[%d] train loss: %f' % (epoch, s)+'\n') 
        
        for j, data in enumerate(testdataloader, 0):
            points, target = data 
            target = target[:,:njoints]
            points, target = points.cuda(), target.cuda()
            
            centroids = points.mean(dim=-2,keepdim=True)
        
            points = points - centroids
            target = target - centroids

            if config["data_augmentation"]["norm"]:
                scale = points.norm(dim=-1).max(dim=-1)[0]
            
                points=points/scale[:,None,None]
                target = target/scale[:,None,None]
            else:
                scale=1
            points = points.transpose(2, 1)
            
            optimizer.zero_grad()
            regressor = regressor.train()
            pred, _, _ = regressor(points)
            print(pred.shape)
            loss = F.mse_loss(pred, target)
            stest+=loss.item()
        
        print('[%d] test loss: %f' % (epoch,stest/len(testdataloader)))
        df.write('[%d] test loss: %f' % (epoch,  stest/len(testdataloader))+'\n')
        
        if(epoch%10==0):
            torch.save(regressor.state_dict(), '%s/model/reg_model_%d.pt' % (outf, epoch))
        scheduler.step()
        
    df.close()
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", type=str,
                        help="Configuration file for the skeleton regressor module, defaults to config_part1.json", default="configurations/config_part1.json")
    
    args = parser.parse_args()
    
    train_skreg(json.load(open(args.config)))