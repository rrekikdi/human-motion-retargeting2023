# Correspondance-free online human motion retargeting
This is the official Pytorch implementation of the paper **Correspondance-free online human motion retargeting**.

- [arXiv](https://arxiv.org/abs/2302.00556) 
- [HAL](https://hal.science/hal-03970689v2)
- [video](https://gitlab.inria.fr/rrekikdi/human-motion-retargeting2023/-/blob/main/doc/Correspondance-free_online_human_retargeting.mp4?ref_type=heads)


 <div align="center">
  <img src='/doc/teaser.mp4' height= 400/>
 </div>
    
## Citation
If you find this project useful for your research, please cite:
```
@inproceedings{rekik2024correspondence,
  title={Correspondence-free online human motion retargeting},
  author={Rekik, Rim and Marsot, Mathieu and Olivier, Anne-H{\'e}l{\`e}ne and Franco, Jean-S{\'e}bastien and Wuhrer, Stefanie},
  booktitle={3DV 2024-11th International Conference on 3D Vision},
  pages={1--10},
  year={2024}
}
```

## Dependencies
- python 3.8.15  
- numpy 1.23.5
- pytorch 1.7.1   
- trimesh 3.18.0    
- human-body-prior 2.1.2  (https://github.com/nghorbani/human_body_prior)

Our code has been tested with Python 3.8.15 , Pytorch 1.7.1 and CUDA 11.7 on ubunto 18.04.6

## Data
This project uses the AMASS dataset, a collection of motion capture datasets from the Max Planck Institute. The data is available at https://amass.is.tue.mpg.de/.
To test our framework, you can dowmload the 4 subsets: ['ACCAD','CMU'] for training, ['SSM'] for validation and ['SFU'] for testing and extract them into the directory ./amass/

Please, also download the extended SMPL-H body model available at https://mano.is.tue.mpg.de and extract the smplh folder.
To prepare the data, please run the code dataset/amass_preprocess.py 

## Training 

We provide the code to have the models that we used in the different stages of our method:
- To train the skeleton regressor module, run python train_1stpart.py 

Please put the selected trained model after convergence in this directory "out/1stpart/reg_model.pth"

- To train the Skeletal motion retargeting module, run python train2ndpart.py

Please put the selected trained model after convergence in this directory "out/2ndpart/model/skrep.pt"

- To train the skinning predictor module, run python train3rdpart.py

Please put the selected trained model after convergence in this directory "out/3rdpart/rend.pth"


## Inference 

model_inference.py is the code to test the full pipeline.



## Acknowledgement 
We used part of codes from pointnet.pytorch (https://github.com/fxia22/pointnet.pytorch) and human_body_prior (https://github.com/nghorbani/human_body_prior).


