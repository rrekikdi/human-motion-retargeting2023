#!/usr/bin/env python
# coding: utf-8

import torch
import torch.utils.data
from utils.representation import *

KINTREE_TABLE = torch.tensor([
[-1,   0,   0,   0,   1,   2,
    3,   4,   5,   6,   7,   8,
    9,   9,   9,  12,  13,  14,
   16,  17,  18,  19],
[  0,   1,   2,   3,   4,   5,
    6,   7,   8,   9,  10,  11,
   12,  13,  14,  15,  16,  17,
   18,  19,  20,  21]]).cuda()

id_to_col = {KINTREE_TABLE[1, i].item(): i for i in range(KINTREE_TABLE.shape[1])}
parent = {
        i: id_to_col[KINTREE_TABLE[0, i].item()]
        for i in range(1, KINTREE_TABLE.shape[1])
}
pack = lambda x: torch.cat([torch.zeros((x.shape[0],4, 3),device=x.device), x.reshape((x.shape[0],4, 1))],dim=-1)
with_zeros = lambda x: torch.cat((x, torch.tensor([[0.0, 0.0, 0.0, 1.0]],device=x.device)[None].repeat(x.shape[0],1,1)),dim=1)

def global_rigid_transformation(pose, J, kintree_table=KINTREE_TABLE):
    results = {}
    bs = pose.shape[0]
    pose = pose.reshape((bs,-1, 3))
    
    results[0] = with_zeros(
        torch.cat((convertPose(pose[...,0, :],AA,MAT).reshape(bs,3,3), J[...,0, :].reshape((bs,3, 1))),dim=-1))
    
    for i in range(1, kintree_table.shape[1]):
       
        results[i] = torch.bmm(results[parent[i]],
            with_zeros(
                torch.cat((convertPose(pose[...,i, :],AA,MAT).reshape(bs,3,3), ((J[:,i, :] - J[:,parent[i], :]
                                                    ).reshape((bs,3, 1)))),dim=-1)))

    

    results = torch.cat([results[i][:,None,...] for i in sorted(results.keys())],dim=1)
    results_global = results

    results2 = [
        results[:,i] - (pack(torch.bmm(results[:,i],torch.cat(((J[:,i, :][...,None]), torch.zeros(bs,1,1,device=J.device)),dim=1))))
        for i in range(22)
    ]
    results = results2
    result = torch.stack(results).transpose(0,1)
    
    return result, results_global


def lbs(pose, v, J, weights, kintree_table=KINTREE_TABLE):
    A, A_global = global_rigid_transformation(pose, J, kintree_table)

    bs,npoints = v.shape[0],v.shape[1]
    rest_shape_h = torch.cat((v, torch.ones((bs,v.shape[1],1),device=v.device)),dim=-1)
   
    T = (weights[...,None,None].repeat(1,1,1,4,4)*A[:,None,...].repeat(1,weights.shape[1],1,1,1)).sum(dim=2)

    v = torch.bmm(T.reshape(-1,4,4),rest_shape_h.reshape(-1,4,1))
    v = v.reshape(bs,npoints,4)[..., :3]
   
    return v


