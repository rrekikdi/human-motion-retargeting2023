#!/usr/bin/env python
import torch
from utils.representation import *
import torch.nn as nn
from models.pointcloud_from_skeleton import *
from models.skeleton_rnn import *
from models.skeleton_from_pointcloud import *
import numpy as np
import os
class RetargetingModel(nn.Module):
    def __init__(self,config,device="cpu"):
        super(RetargetingModel, self).__init__()
        
        self.kintree_table = torch.tensor([
        [-1,0,0,0,1,2,3,4,5,6,7,8,9,9,9,12,13,14,16,17,18,19],
        [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21]]).to(device)
        
        self.njoints = self.kintree_table.shape[1]
        self.locomotion_retargeting_module = SkeletonReposing(self.kintree_table[0]).eval().to(device)
        self.skeleton_regressor = PointNetRegression(output_size=3, k=self.njoints).eval().to(device)
        self.skinning_module = RiggingRenderer(skeleton_regressor=self.skeleton_regressor ,num_joints=self.njoints,kintree_table=self.kintree_table).eval().to(device)
        
        self.locomotion_retargeting_module.load_state_dict(
            torch.load(os.path.join(config["module_2"]["out_folder"],config["module_2"]["state_dict_file"])))
        self.skeleton_regressor.load_state_dict(
            torch.load(os.path.join(config["module_1"]["out_folder"],config["module_1"]["state_dict_file"])))
        self.skinning_module.load_state_dict(
            torch.load(os.path.join(config["module_3"]["out_folder"],config["module_3"]["state_dict_file"])))
    
    
    def get_floor_height(self,skeleton):
        
        fid_l, fid_r = [8,11],[7,10]
        foot_heights = torch.minimum(skeleton[:,:,fid_l,2],
                                skeleton[:,:,fid_r,2]).min(dim=2)[0]
        floor_height = foot_heights.min(dim=1)[0]

        #print(floor_height)

        return floor_height
    
  
    def forward(self,source_motion,target_tpose,convertsmpl2amass=(False,True)):
        
        bs,nof,npoints,_ = source_motion.shape
        
        # source skeletons should be in amass referential frame
        if convertsmpl2amass[0]:
            source_motion = smpl2amass(source_motion)
        
        source_centroids = source_motion.mean(dim=-2,keepdim=True)
        self.source_points = source_motion - source_centroids
        self.source_points = self.source_points.reshape(bs*nof,-1,3)
        self.source_skeleton,_,_ = self.skeleton_regressor(self.source_points.transpose(2, 1))
        self.source_points = self.source_points.reshape(bs,nof,-1,3) + source_centroids
        self.source_skeleton = self.source_skeleton.reshape(bs,nof,-1,3) + source_centroids
        
        
        if convertsmpl2amass[1]:
            target_tpose = smpl2amass(target_tpose)
        
        target_centroids = target_tpose.mean(dim=-2,keepdim=True)
        target_tpose = target_tpose - target_centroids
        

        target_tpose_skeleton,_,_ = self.skeleton_regressor(target_tpose.transpose(2, 1))
        target_tpose_skeleton = target_tpose_skeleton + target_centroids
        
        # tposes should be in smpl referential frame
        target_tpose_skeleton = amass2smpl(target_tpose_skeleton- target_tpose_skeleton[:,0:1])
        target_tpose = amass2smpl(target_tpose- target_tpose_skeleton[:,0:1])
        self.target_tpose = target_tpose_skeleton
        # translation in amass ref
        translation =  (self.source_skeleton[:,:,0:1,:])
        init_frame_translation = translation[:,0:1].clone()
        translation = translation- init_frame_translation
        # translation = translation- translation[:,0:1]
        
        #source_skeletons_nt = self.source_skeleton - self.source_skeleton[:,:,0:1,:].clone() 
        
        source_skeletons_nt,translation_nr,target_tpose_skeleton,_,norm_rot_input = self.locomotion_retargeting_module.preprocess(self.source_skeleton,target_tpose_skeleton,None)
        
        skeleton_retar,rot_retar,trans_retar  = self.locomotion_retargeting_module(source_skeletons_nt,translation_nr,target_tpose_skeleton)
        
        skeleton_retar_unorm,trans_retar_unorm, rot_retar_unorm = self.locomotion_retargeting_module.unprocess(
                                                                        skeleton_retar,trans_retar,rot_retar,norm_rot_input)
        rot_retar_unorm = convertPose(rot_retar_unorm,QUAT,AA)
        retargeted_target = self.skinning_module(
            rot_retar_unorm.reshape(bs*nof,-1,3),
            target_tpose.repeat(nof,1,1),
            tposes=target_tpose_skeleton.repeat(nof,1,1))
        
        
        retargeted_target_wt = retargeted_target.reshape(bs,nof,-1,3) + trans_retar_unorm.reshape(bs,nof,-1,3) 
        #retargeted_target_wt = retargeted_target_wt + init_frame_translation 
        floor_height_retar = self.get_floor_height(skeleton_retar_unorm+trans_retar_unorm.reshape(bs,nof,-1,3))
        self.retar_skeleton = skeleton_retar_unorm+trans_retar_unorm.reshape(bs,nof,-1,3)  - floor_height_retar
        retargeted_target_wt[...,2] -= floor_height_retar
        
        
        return retargeted_target_wt