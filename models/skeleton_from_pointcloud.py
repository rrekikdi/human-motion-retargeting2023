#!/usr/bin/env python
# coding: utf-8
import torch.nn as nn
import torch.nn.functional as F
from models.pointnet import PointNetfeat

class PointNetRegression(nn.Module):
    def __init__(self, output_size, k, feature_transform=False):
        super(PointNetRegression, self).__init__()
        self.k = k
        self.output_size = output_size
        self.feature_transform = feature_transform
        self.feat = PointNetfeat(global_feat=True, feature_transform=feature_transform)
        self.fc1 = nn.Linear(1024, 512)
        self.fc2 = nn.Linear(512, 256)
        self.fc3 = nn.Linear(256, self.k*self.output_size)
        self.dropout = nn.Dropout(p=0.3)
        self.bn1 = nn.BatchNorm1d(512)
        self.bn2 = nn.BatchNorm1d(256)
        self.relu = nn.ReLU()

    def forward(self, x):
        batchsize = x.size()[0]
        x, trans, trans_feat = self.feat(x)
        x = F.relu(self.bn1(self.fc1(x)))
        x = F.relu(self.bn2(self.dropout(self.fc2(x))))
        x = self.fc3(x)
        x = x.view(batchsize,self.k,self.output_size)
        return x, trans, trans_feat