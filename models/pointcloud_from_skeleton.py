from nntplib import NNTP
import torch
import torch.nn as nn
from models.skinning import lbs
from utils.coordinates import *
from models.pointnet import PointNetfeat

class RiggingField(nn.Module):
    def __init__(self,num_joints):
        super(RiggingField, self).__init__()
        
        self.num_joints = num_joints
        feat_dim = 1024
        # self.global_pose_feature = PointNetfeat(global_feat=True, feature_transform=False)
        self.network = nn.Sequential(
            nn.Linear(num_joints*3,256),
            nn.ReLU(),
            nn.Dropout(p=0.2),
            nn.Linear(256,256),
            nn.ReLU(),
            nn.Dropout(p=0.2),
            nn.Linear(256,num_joints),
            nn.Softmax(dim=-1)
        )
        
    def forward(self,pointcloud_target,target_joints):
        # pointcloud_target BSxNx3, target_joints BSxnum_jointsx3
        bs = target_joints.shape[0]
        
        pointcloud_target = pointcloud_target.reshape(bs,pointcloud_target.shape[1],1,3).repeat(1,1,self.num_joints,1)
        target_joints_repeated = target_joints.reshape(bs,1,self.num_joints,3).repeat(1,pointcloud_target.shape[1],1,1)
        
        # compute distance matrix between point and joints, make the network translation invariant
        # TODO : Use euclidean norm to make it rotation invariant as well
        dist_mat  = (pointcloud_target-target_joints_repeated).reshape(bs,pointcloud_target.shape[1],-1)
        dmat = ((dist_mat.reshape(bs,pointcloud_target.shape[1],self.num_joints,3))**2).sum(dim=-1)
        
        # global_feat, _, _ = self.global_pose_feature(pointcloud_target-pointcloud_target.mean(dim=-2,keepdim=True))
        # global_feat = global_feat[:,None,:].repeat(1,pointcloud_target.shape[1])
        pseudo_rigging = self.network(dist_mat)
        
        # nn_rigging consists in associating each point to its closest joint, is used as an initialization rigging
        dmat = ((dist_mat.reshape(bs,pointcloud_target.shape[1],self.num_joints,3))**2).sum(dim=-1)
        boolidx = dmat == torch.min(dmat,dim=-1,keepdim=True)[0]
        nn_rigging = torch.zeros(bs,pointcloud_target.shape[1],self.num_joints,device=pointcloud_target.device).float()
        nn_rigging[boolidx] = 1.0
        
        return  pseudo_rigging,nn_rigging
        
class RiggingRenderer(nn.Module):
    def __init__(self, skeleton_regressor,num_joints,kintree_table,skinning_fn=lbs):
        """this module encapsulates a rigging field, a skeleton regressor and a skinning function
        It can repose an unstructured pointcloud using the three

        Args:
            skeleton_regressor (_type_): _description_
            num_joints (_type_): _description_
            kintree_table (_type_): _description_
        """
        super(RiggingRenderer, self).__init__()
        
        self.skeleton_regressor = skeleton_regressor
        self.num_joints = num_joints
        self.kintree_table = kintree_table
        self.skinning_fn = skinning_fn
        self.rigging_field = RiggingField(num_joints=num_joints)
        
        self.fine_tune = False
        self.decoder = nn.Sequential(
            nn.Linear(69,256),
            nn.ReLU(),
            nn.Dropout(p=0.2),
            nn.Linear(256,256),
            nn.ReLU(),
            nn.Dropout(p=0.2),
            nn.Linear(256,3)
        )
        
    
    def skinning(self,pointcloud_target,pseudo_rigging_weights,tpose_joints,rotation_target_reposed):
        
        outs = lbs(rotation_target_reposed,pointcloud_target, tpose_joints, pseudo_rigging_weights , self.kintree_table)
            
        return outs
        
    def forward(self, rotation_target_reposed,pointcloud_target,tposes=None):
        
        bs,num_points = pointcloud_target.shape[0],pointcloud_target.shape[1]
        
        # If Tpose is not given, extract it from target
        if tposes is None:
            # Assuming target is in rest SMPL coordinate frame, and that skeleton regressor has been trained in centered AMASS coordinate frame
            
            # convert pointcloud to centered AMASS coordinate frame
            self.pointcloud_target = smpl2amass(pointcloud_target.clone())
            centroid = self.pointcloud_target.mean(dim=-2,keepdim=True)
            self.tpose_joints,_,_ = self.skeleton_regressor(
                (self.pointcloud_target-centroid).transpose(-1,-2))
            self.tpose_joints = self.tpose_joints + centroid
            # revert back to SMPL frame
            self.pointcloud_target = pointcloud_target
            self.tpose_joints = amass2smpl(self.tpose_joints) 
           
            
        else:
            self.tpose_joints = tposes[:,:self.num_joints]
            self.pointcloud_target = pointcloud_target
        
        # predict rigging weights
        self.pseudo_rigging,self.nn_rigging = self.rigging_field(self.pointcloud_target,self.tpose_joints)
        
        reposed_pointcloud = self.skinning(self.pointcloud_target, self.pseudo_rigging,self.tpose_joints,rotation_target_reposed) 

        return reposed_pointcloud
