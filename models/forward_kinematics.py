import torch
import torch.nn as nn
from utils.representation import *

class FK(nn.Module):
    """Forward kinematics module for pytorch """
    
    def __init__(self,parents):
        super(FK,self).__init__()
        self.parents=parents

    def transforms_multiply(self, t0s, t1s):
        return torch.bmm(t0s, t1s)

    def transforms_blank(self, rotations):
        diagonal = torch.diag(torch.tensor([1.0, 1.0, 1.0, 1.0],device=rotations.device))[None, None, :, :]
        ts = diagonal.repeat([int(rotations.shape[0]),int(rotations.shape[1]), 1, 1])
        return ts

    def transforms_rotations(self, rotations):
        """Unit quaternion based rotation matrix computation"""
        q_length = torch.sqrt(torch.sum(torch.square(rotations), dim=-1))
        qw = rotations[..., 0] / q_length
        qx = rotations[..., 1] / q_length
        qy = rotations[..., 2] / q_length
        qz = rotations[..., 3] / q_length
       
        x2 = qx + qx
        y2 = qy + qy
        z2 = qz + qz
        xx = qx * x2
        yy = qy * y2
        wx = qw * x2
        xy = qx * y2
        yz = qy * z2
        wy = qw * y2 
        xz = qx * z2
        zz = qz * z2
        wz = qw * z2
        
        dim0 = torch.stack([1.0 - (yy + zz), xy - wz, xz + wy], dim=-1)
        dim1 = torch.stack([xy + wz, 1.0 - (xx + zz), yz - wx], dim=-1)
        dim2 = torch.stack([xz - wy, yz + wx, 1.0 - (xx + yy)], dim=-1)
        m = torch.stack([dim0, dim1, dim2], dim=-2)
        
        return m

    def transforms_local(self, positions, rotations):

        transforms = self.transforms_rotations(rotations)
        
        positions_rel = positions.clone()
        positions_rel[:,1:,...] = positions_rel[:,1:,...]-positions_rel[:,self.parents[1:],...]
        transforms = torch.cat(
                [transforms, positions_rel[:, :, :, None]], dim=-1)
        zeros = torch.zeros(
                [int(transforms.shape[0]),
                 int(transforms.shape[1]), 1, 3],device=positions.device)
        ones = torch.ones([int(transforms.shape[0]), int(transforms.shape[1]), 1, 1],device=positions.device)
        zerosones = torch.cat([zeros, ones], dim=-1)
        transforms = torch.cat([transforms, zerosones], dim=-2)
        return transforms

    def transforms_global(self, parents, positions, rotations):
        
        _locals = self.transforms_local(positions, rotations)
        
        _globals = self.transforms_blank(rotations)
       
        _globals = torch.cat([_locals[:, 0:1], _globals[:, 1:]], dim=1)
        
        _globals = list(torch.split(_globals, 1, dim=1))
        
        for i in range(1, positions.shape[1]):
        
            _globals[i] = self.transforms_multiply(_globals[parents[i]][:, 0],_locals[:, i])[:, None, :, :]

        return torch.cat(_globals, dim=1)

    def forward(self, positions, rotations):

        positions = self.transforms_global(self.parents, positions,rotations)[:, :, :, 3]
        return positions[:, :, :3] / positions[:, :, 3, None]