import torch
import torch.nn as nn
from models.forward_kinematics import FK
import torch.nn.functional as F
import copy 
from utils.representation import *
import torchvision.transforms as T


class Discriminator(nn.Module):
    """ Adversarial discriminator
    """
    def __init__(self,njoints=22):
        super(Discriminator, self).__init__()
        """njoints is the number of SMPL joints to consider
        """
        self.conv1 = nn.Conv1d(njoints*3+3,128,kernel_size=5,padding=2)
        self.conv2 = nn.Conv1d(128,256,kernel_size=5,padding=2)
        self.conv3 = nn.Conv1d(256+njoints*3,512,kernel_size=5,padding=2)
        self.conv4 = nn.Conv1d(512+njoints*3,1024,kernel_size=5,padding=2)
        self.conv5 = nn.Conv1d(1024+njoints*3,1,kernel_size=5,padding=2)

        self.norm1 = nn.BatchNorm1d(256)
        self.norm2 = nn.BatchNorm1d(512)
        self.norm3 = nn.BatchNorm1d(1024)
        
        self.fake_score = 1
        self.margin = 0.3
        
    def forward(self,_input,_cond):
        
        x0 = _input.reshape(_input.shape[0],_input.shape[1],-1).transpose(1,2)
        x0 = self.conv1(x0)
        x1 = nn.ReLU()(x0)
        x2 = self.conv2(x1)
        x3 = nn.ReLU()(x2)
        x4 = self.norm1(x3)
        c1 =  _cond.reshape(_cond.shape[0],-1,1).repeat(1,1,x4.shape[-1])
        x5 = torch.cat((x4,c1),dim=1)
        
        x6 = self.conv3(x5)
        x7 = nn.ReLU()(x6)
        x8 = self.norm2(x7)
        c2 =  _cond.reshape(_cond.shape[0],-1,1).repeat(1,1,x8.shape[-1])
        x9 = torch.cat((x8,c2),dim=1)
        
        x10 = self.conv4(x9)
        x11 = nn.ReLU()(x10)
        x12 = self.norm3(x11)
        c3 =  _cond.reshape(_cond.shape[0],-1,1).repeat(1,1,x11.shape[-1])
        x13 = torch.cat((x12,c3),dim=1)
        
        logits = self.conv5(x13)
        
        return logits

        
class SkeletonReposing(nn.Module):
    """This is the locomotion retargetting module"""
    def __init__(self,parents,hidden_size=512,num_layers=2,dropout=0.1):
        super(SkeletonReposing, self).__init__()

        self.parents = parents
        self.rnn_enc = nn.GRU(3*(len(parents)+1),hidden_size,num_layers,dropout=dropout,batch_first=True)
        self.rnn_dec = nn.GRU(3*(len(parents))+hidden_size,hidden_size,num_layers,dropout=dropout,batch_first=True)
        self.to_quat = nn.Linear(hidden_size,4*len(parents))
        self.totrans = nn.Linear(hidden_size,3)
        self.forward_kinematics =FK(parents)
        self.bce = torch.nn.BCEWithLogitsLoss()
        self.beta = 0.001
        
    def get_skel(self,joints, parents):
        c_offsets = []
        for j in range(parents.shape[0]):
            if parents[j] != -1:
                c_offsets.append(joints[j,:] - joints[parents[j],:])
            else:
                c_offsets.append(joints[j,:])
        return torch.stack(c_offsets, dim=0)

    def softmax(self,x, **kw):
        softness = kw.pop("softness", 1.0)
        maxi, mini = torch.max(x, **kw)[0], torch.min(x, **kw)[0]
        return maxi + torch.log(softness + torch.exp(mini - maxi))

    def softmin(self,x, **kw):
        return -self.softmax(-x, **kw)

    def preprocess(self,skeletons,tposes,rot=None):
        
        skeletons_wt = skeletons[:,:,:len(self.parents),:]
        tposes = tposes[:,:len(self.parents),:]
      
        skeletons_nt = skeletons_wt.clone()
        skeletons_nt =  skeletons_nt-  skeletons_nt[:,:,0:1].clone()
        
        translations =  (skeletons_wt[:,:,0:1,:]).clone()
       
        bs,nof = skeletons.shape[:2]
        
        """ Get Forward Direction """
        sdr_l, sdr_r, hip_l, hip_r = 16, 17, 1, 2
        across1 = skeletons_nt[:,:,hip_l] - skeletons_nt[:,:,hip_r]
        across0 = skeletons_nt[:,:,sdr_l] - skeletons_nt[:,:,sdr_r]
        across = across0 + across1
        across = across / torch.sqrt((across**2).sum(dim=-1,keepdim=True))
       
        # Z is up
        up = torch.tensor([[[0,0,1]]],
            dtype=torch.float,
            device=across.device).repeat(bs,nof,1)
        forward = torch.cross(across,up)
        forward = forward / torch.sqrt((forward**2).sum(dim=-1))[...,None]
    
        """ Remove Up Rotation """
        target = torch.tensor([[[0.0,-1.0,0.0]]],device=forward.device).repeat(bs,nof, 1)
        dot = (forward*target).sum(dim=-1,keepdim=True)
       
        a = torch.cross(forward, target,dim=-1)
        
        w = torch.sqrt((forward**2).sum(dim=-1) * (target**2).sum(dim=-1)) + dot[...,0]
        rotations = torch.cat([w[...,None], a], dim=-1)
        rotations = rotations / (rotations**2).sum(dim=-1,keepdim=True)**0.5
    
   
        rotations_mat = convertPose(rotations,QUAT,MAT)
        skeletons_nt = torch.bmm(
            rotations_mat[:,:,None].repeat(1,1,skeletons_nt.shape[2],1).reshape(-1,3,3), 
            skeletons_nt.reshape(-1,3,1)
            ).reshape(bs,nof,-1,3)
       
        translations  = torch.bmm(
            rotations_mat[:,:,None].repeat(1,1,translations.shape[2],1).reshape(-1,3,3), 
            translations.reshape(-1,3,1)
            ).reshape(bs,nof,-1,3)
        
        if rot is not None:
            poses_mat =  convertPose(rot[:,:,:len(self.parents)],AA,MAT).float().reshape(bs,nof,-1,3,3)
            poses_mat[:,:,0] =  torch.bmm(
                rotations_mat.reshape(-1,3,3), 
                poses_mat[:,:,0].reshape(-1,3,3)
                ).reshape(bs,nof,3,3)
            poses_mat = poses_mat.reshape(bs,nof,-1,9)
            poses_quat = convertPose(poses_mat,MAT,QUAT).detach().requires_grad_(True)
        else:
            poses_quat = None
          
        return (skeletons_nt.detach().requires_grad_(True), 
                translations.detach().requires_grad_(True),
                tposes.detach().requires_grad_(True),
                poses_quat,
                rotations.detach().requires_grad_(True))
    
    def unprocess(self,skeletons,translations,rotations,norm_rot_input):
        
        bs,nof = skeletons.shape[:2]
        skeletons = torch.bmm(
                convertPose(norm_rot_input.clone(),QUAT,MAT)[:,:,None].repeat(1,1,skeletons.shape[2],1).reshape(-1,3,3).transpose(-1,-2), 
                skeletons.reshape(-1,3,1)
                ).reshape(bs,nof,-1,3)
        
        translations  = torch.bmm(
        convertPose(norm_rot_input.clone(),QUAT,MAT)[:,:,None].repeat(1,1,translations.shape[2],1).reshape(-1,3,3).transpose(-1,-2), 
        translations.reshape(-1,3,1)
        ).reshape(bs,nof,-1,3)

        norm_rot_input_mat = convertPose(norm_rot_input,QUAT,MAT)
        
    
        rotations_mat =  convertPose(rotations,QUAT,MAT).reshape(bs,nof,-1,3,3)
       
        rotations_mat[:,:,0] =  torch.bmm(
            norm_rot_input_mat.reshape(-1,3,3).transpose(-1,-2), 
            rotations_mat[:,:,0].reshape(-1,3,3)
            ).reshape(bs,nof,3,3)
        rotations_mat = rotations_mat.reshape(bs,nof,-1,9)
        rotations_quat = convertPose(rotations_mat,MAT,QUAT)
            
        return skeletons, translations,rotations_quat
        
    def forward(self,skeletons,translations,tpose):
        bs,nof,njoints = skeletons.shape[:3]
    
        # remove velocities of root joint
        tpose = tpose - tpose[:,0:1,:]
        
   
        tpose = tpose[:,None,...].repeat(1,nof,1,1)
        feat,hidden_enc_state = self.rnn_enc(torch.cat([skeletons,translations],dim=-2).reshape(bs,nof,-1))
       
        self.data_b,hidden_dec_state = self.rnn_dec(torch.cat((tpose.reshape(bs,nof,-1),feat),dim=-1))
        
        self.trans_b = self.totrans(self.data_b).reshape(bs,nof,1,3)
        self.angles_b = self.to_quat(self.data_b).reshape(bs,nof,-1,4)
        self.angles_b = self.normalized(self.angles_b)
    
        self.skel_B = self.forward_kinematics(tpose.reshape(bs*nof,-1,3),self.angles_b.reshape(bs*nof,-1,4)) 
        self.skel_B = self.skel_B.reshape(bs,nof,-1,3)
        
        return self.skel_B,self.angles_b,self.trans_b
    
    def normalized(self, angles):
        lengths = torch.sqrt(torch.sum(torch.square(angles), dim=-1))
        return angles / lengths[..., None]
    
    def euler(self, angles, order="yzx"):
        q = self.normalized(angles)
        
        q0 = q[..., 0]
        q1 = q[..., 1]
        q2 = q[..., 2]
        q3 = q[..., 3]

        if order == "xyz":
            ex = torch.atan2(2 * (q0 * q1 + q2 * q3), 1 - 2 * (q1 * q1 + q2 * q2))
            ey = torch.asin(torch.clamp(2 * (q0 * q2 - q3 * q1), -1, 1))
            ez = torch.atan2(2 * (q0 * q3 + q1 * q2), 1 - 2 * (q2 * q2 + q3 * q3))
            return ex[:, :, 1:]
        elif order == "yzx":
          
            ex = torch.atan2(2 * (q1 * q0 - q2 * q3),
                        -q1 * q1 + q2 * q2 - q3 * q3 + q0 * q0)
            ey = torch.atan2(2 * (q2 * q0 - q1 * q3),
                        q1 * q1 - q2 * q2 - q3 * q3 + q0 * q0)
            ez = torch.asin(torch.clamp(2 * (q1 * q2 + q3 * q0), -1, 1))
            #ex pour smpl forward kinematics, ey pour mixamo
            return ey[:, :, 1:]
        else:
            raise Exception("Unknown Euler order!")
    
    def compute_loss(self,params,discriminator=None):
        
        # initialize to predict identity quaternion
        if params["init"]:
            zero_rot = torch.zeros_like(params["rot_retar"])
            zero_rot[...,0]=1.0
            return (
                F.mse_loss(params["rot_retar"],zero_rot)+F.mse_loss(params["rot_cycon"],zero_rot),
                torch.tensor(0.0,device=params["pos_retar"].device,requires_grad=True),
                torch.tensor(0.0,device=params["pos_retar"].device,requires_grad=True))
            
        # if we want adversarial training
        if discriminator is not None:
            
            # find inputs where target shape is similar to source shape
            similar = params["tpos_input"][params["mix_indexes"]] == params["tpos_input"]
            similar = similar.all(dim=-1).all(dim=-1)

            # compute gen_loss without storing grad for the discriminator network
            if similar.all():
                gen_loss = torch.tensor(0.0,device=params["pos_retar"].device)
            else:
                dnograd = copy.deepcopy(discriminator).requires_grad_(False)
                realism_no_grad = dnograd(
                torch.cat([params["trans_retar"][:,:-1],
                (params["pos_retar"][:,1:]-params["pos_retar"][:,:-1])],dim=-2),
                params["tpos_input"][params["mix_indexes"]])
                gen_loss = self.beta * self.bce(realism_no_grad[~similar],
                                torch.ones_like(realism_no_grad[~similar]))
               
            if similar.any():
                gen_loss =gen_loss+F.mse_loss(params["pos_retar"][similar],params["pos_input"][similar])+\
                    F.mse_loss(params["trans_retar"][similar],params["trans_input"][similar])
                    
            # compute disc_loss without storing grad for the generator network
            realism_retar = discriminator(
                torch.cat([params["trans_retar"][:,:-1],
                (params["pos_retar"][:,1:]-params["pos_retar"][:,:-1])],dim=-2).detach(),
                (params["tpos_input"][params["mix_indexes"]]).detach())
            realism_input = discriminator(
                torch.cat([params["trans_input"][:,:-1],
                (params["pos_input"][:,1:]-params["pos_input"][:,:-1])],dim=-2).detach(),
                params["tpos_input"].detach())
               
            adv_loss_real = self.bce(
                realism_input, torch.ones_like(realism_input))
            adv_loss_fake = self.bce(
                realism_retar, torch.zeros_like(realism_retar))

            disc_loss = adv_loss_real + adv_loss_fake

            discriminator.cur_score = nn.Sigmoid()(realism_retar)[~similar].mean().detach()
                
        else:
            gen_loss = torch.tensor(0.0,device=params["pos_retar"].device)
            disc_loss= torch.tensor(0.0,device=params["pos_retar"].device)
        
        cycle_consistency_loss = F.mse_loss(params["pos_input"],params["pos_cycon"])+\
                                    F.mse_loss(params["trans_input"],params["trans_cycon"]) +\
                                    params["rot_weight"] * F.mse_loss(params["rot_input"],params["rot_cycon"])
       
        interm_smooth = torch.sum(
            (params["trans_retar"][:, 2:] - 2*params["trans_retar"][:, 1:-1]+ params["trans_retar"][:, :-2])**2)
        cycle_smooth = torch.sum(
          (params["trans_cycon"][:, 2:] - 2*params["trans_cycon"][:, 1:-1]+ params["trans_cycon"][:, :-2])**2)
        smoothness_loss =  0.5 * (interm_smooth + cycle_smooth)
        
        
        rads = params["twist_threshold"]*3.141 / 180.0
        euler_retar = self.euler(params["rot_retar"],order="xyz")
        euler_cycon = self.euler(params["rot_cycon"],order="xyz")
        twist_loss = torch.square(
              torch.maximum(
                  torch.zeros_like(euler_retar[:,:,1:]),
                  torch.abs(euler_retar[:,:,1:]) - rads )).mean() + torch.square(
              torch.maximum(
                  torch.zeros_like(euler_cycon[:,:,1:]),
                  torch.abs(euler_cycon[:,:,1:]) - rads )).mean()
        
        return cycle_consistency_loss + params["twist_weight"]*twist_loss + gen_loss+ params["smooth_weight"]*smoothness_loss,gen_loss ,disc_loss
       