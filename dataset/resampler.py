import torch
import trimesh
import numpy as np


class Resampler:
    """Used to resampled the SMPL pointclouds to the uniform topology using barycenters and faces
    """
    def __init__(self,fc_path,coords_path,indices_path,faces_rm_path="/morpheo-nas/datarim/fixed_rm_faces.pt"):
        
        self.fc = torch.load(fc_path)
        self.bary_coords = torch.load(coords_path)
        self.face_indices = torch.load(indices_path)
        self.triangles = None
        
    def sample_points_give_bary(self,mesh_source):
        
        samples_triangles = mesh_source.triangles[self.face_indices]
        p = self.bary_coords[..., None] * samples_triangles
        p = np.sum(p, axis=1)
        return p, samples_triangles

    def resample(self,pointcloud):
        mesh = trimesh.Trimesh(pointcloud,self.fc)
        pointcloud_resampled, samples_triangles = self.sample_points_give_bary(mesh)
        return torch.from_numpy(pointcloud_resampled).float()
    