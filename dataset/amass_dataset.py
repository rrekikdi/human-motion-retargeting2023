import torch
import torch.utils.data
import os
import torch
from torch.utils.data import Dataset
import math
import numpy as np
import random
import math

# These keys correspond to different input types
PCD = 0             # Pointclouds, randomly resampled to be uniform
T_RS = 1            # Tpose poincloud, randomly resampled to be uniform 
PCD_SMPL = 2        # Pointclouds, SMPL vertices
RM_PCD = 3          # Pointclouds, remeshed with uniform topology NOT RANDOM
RM_T = 4            # Tpose poincloud, remeshed with uniform topology NOT RANDOM
T_PC = 5            # Tpose poincloud, SMPL vertices
SK = 6              # SMPL skeletons, from AMASS
T_SK = 7            # Tpose SMPL skeletons
SK_REG = 8          # SMPL skeletons, predicted with Regressor module (see generate_skreg.py)
T_SK_REG = 9        # Tpose SMPL skeletons, predicted with Regressor module (see generate_skreg.py)
POSE = 10           # AMASS pose parameters, axis angle (rodrigues) representation
TRANS = 11          # AMASS translation parameters (up axis is Z)   /!\ SHOULD ALWAYS BE HIGHER KEY VALUE see "out" list in get_sequential())

SPLITS = {
    'train':['ACCAD','CMU'],
    'vald':['SSM_synced'],
    'test':['SFU']
}

class AmassDataset(Dataset):
    def __init__(self,
                 data_path,
                 split ='train',
                 sequential=False,
                 fps = 30,
                 num_frames=60,
                 keys=[PCD,T_PC],
                 shuffle_point=False):
        """_summary_

        Args:
            data_path (str): path to data folder 
            split (str, optional):Split 'train','validation' or 'test'. Defaults to 'train'.
            sequential (bool, optional): Whether or not to consider sequential data . Defaults to False.
            num_frames (int, optional): If sequential is true, number of frames per sequence. Defaults to 60.
            keys (list, optional): Data format to return, see keys comments from this file for more details. Defaults to [PCD,T_PC].
            shuffle_point (bool, optional): Whether or not to shuffle remeshed data to simulate unstructured scans. Defaults to False.
        """
        
        self.keys = keys
        self.kb = torch.tensor([(i in self.keys) for i in range(TRANS+1)]) # bool tensor for easy key selection
        
        self.data_path = data_path
        self.split = split
        
        # sequential data loaded very time
        self.seq_data = {
            'names':[],
            'fps':[],
            'nof':[],
            'pointcloud':[],
            'skeleton':[],
            'pose':[],
            'trans':[],
            'skeleton_regressed':[],
            'tpose_regressed':[],
            'tpose_sk':[],
            'tpose_pc':[],
            'tpose_rs':[],
            'pc_smpl':[],
            'remeshed_pointcloud':[],
            'remeshed_tpose':[]
        }
        
        # static indices, loaded only if sequential is False
        self.data = {
                     "seq_idx":[],
                     "frame_idx":[]
                     }
       
        self.num_frames=num_frames
        self.sequential = sequential 
        self.shuffle_point = shuffle_point
        self.fps = fps
        
        if split not in SPLITS.keys():
            print("unexpected Split")
        
        folders = [os.path.join(data_path,subset) for subset in SPLITS[self.split]]
       
        for folder in folders:
            for r,d,f in os.walk(folder):
                
                if len(f)>0:
                    try:
                        seq_fps = torch.load(os.path.join(r,"fps.pt"))
                        seq_frames = torch.load(os.path.join(r,"nof.pt"))
                        seq_duration = seq_frames/seq_fps
                    except:
                        print("Error  with path %s"%r)
                        continue
                    
                    # remove sequences that are not long enough 
                    if(self.sequential and (seq_duration<=self.num_frames/fps)):
                        continue
                    
                    self.seq_data['names'].append(r.replace(data_path,"").replace(os.sep,"_"))
                    self.seq_data['fps'].append(int(seq_fps))
                    self.seq_data['nof'].append(int(seq_frames))
                    
                    if self.kb[PCD]:
                        self.seq_data['pointcloud'].append(torch.from_file(
                                os.path.join(r,"pointcloud.pt"),
                                dtype=torch.float32,
                                size=seq_frames*6890*3).reshape(seq_frames,6890,3))
                      
                    if self.kb[SK]: 
                        self.seq_data['skeleton'].append(torch.from_file(
                                os.path.join(r,"skeleton.pt"),
                                dtype=torch.float32,
                                size=seq_frames*52*3).reshape(seq_frames,52,3))
                    if self.kb[SK_REG]:   
                        self.seq_data['skeleton_regressed'].append(torch.from_file(
                                os.path.join(r,"skeleton_regressed.pt"),
                                dtype=torch.float32,
                                size=seq_frames*22*3).reshape(seq_frames,22,3))
                    if self.kb[T_SK_REG]:   
                        self.seq_data['tpose_regressed'].append(torch.from_file(
                                os.path.join(r,"tpose_regressed.pt"),
                                dtype=torch.float32,
                                size=22*3).reshape(22,3))
                        
                    if self.kb[T_PC]:  
                        self.seq_data['tpose_pc'].append(torch.from_file(
                                os.path.join(r,"tpose.pt"),
                                dtype=torch.float32,
                                size=6890*3).reshape(6890,3))
           
                        
                    if self.kb[T_RS]:
                        self.seq_data['tpose_rs'].append(torch.from_file(
                                os.path.join(r,"tpose_rs.pt"),
                                dtype=torch.float32,
                                size=6890*3).reshape(6890,3))
                        
                    if self.kb[T_SK]:
                        self.seq_data['tpose_sk'].append(torch.from_file(
                                os.path.join(r,"tpose_skeleton.pt"),
                                dtype=torch.float32,
                                size=52*3).reshape(52,3))
                    
                    if self.kb[PCD_SMPL]:
                        self.seq_data['pc_smpl'].append(torch.from_file(
                                os.path.join(r,"smpl_pointcloud.pt"),
                                dtype=torch.float32,
                                size=seq_frames*6890*3).reshape(seq_frames,6890,3))
                        
                    if self.kb[RM_T]:
                        self.seq_data['remeshed_tpose'].append(torch.from_file(
                                os.path.join(r,"remeshed_tpose.pt"),
                                dtype=torch.float32,
                                size=6890*3).reshape(6890,3))
                    if self.kb[RM_PCD]:
                        self.seq_data['remeshed_pointcloud'].append(torch.from_file(
                                os.path.join(r,"remeshed_pointcloud.pt"),
                                dtype=torch.float32,
                                size=seq_frames*6890*3).reshape(seq_frames,6890,3))
                    if self.kb[POSE]:
                        self.seq_data['pose'].append(torch.from_numpy
                            (np.load(os.path.join(r,"pose30fps.npz"))['poses']).reshape(seq_frames,52,3))
                    if self.kb[TRANS]:
                        self.seq_data['trans'].append(torch.from_numpy
                            (np.load(os.path.join(r,"pose30fps.npz"))['trans']).reshape(seq_frames,3))
        
        # for static data, change indexing
        if(not self.sequential):
            for seq_idx,(nof,fps) in enumerate(zip(self.seq_data['nof'],self.seq_data['fps'])):      
                for frame_idx in range(fps,int(nof-fps/2),int(fps/2)):
                    self.data["seq_idx"].append(seq_idx)
                    self.data["frame_idx"].append(frame_idx)

        print(self.__len__(), " data points loaded") 
    
   
    def get_sequential(self,index):
        
        out = [None for i in range(TRANS+1)]
        
        nof = self.seq_data['nof'][index] # number of frames
        
        fps_ratio = 1.0 # used to temporally resample sequences to reach the desired framerate
        if(self.fps is not None):
            fps_ratio = self.seq_data['fps'][index]/float(self.fps)
        
        rnof = math.ceil(self.num_frames*fps_ratio) # compute number of frames of resampled sequence
        
        # take a random interval in the temporally resampled sequence
        start = random.randint(0,(nof-rnof)-1)
        end = start+ rnof

        sampling = torch.linspace(0,rnof-fps_ratio,self.num_frames).long()
  
        if self.kb[PCD]:
            out[PCD] = self.seq_data['pointcloud'][index][start:end][sampling]
        if self.kb[SK]: 
            out[SK] = self.seq_data['skeleton'][index][start:end][sampling]
        if self.kb[SK_REG]:
            out[SK_REG] = self.seq_data['skeleton_regressed'][index][start:end][sampling]
        if self.kb[T_SK_REG]:
            out[T_SK_REG] = self.seq_data['tpose_regressed'][index]                                             
        if self.kb[T_PC]:       
            out[T_PC] = self.seq_data['tpose_pc'][index]      
        if self.kb[T_RS]:
            out[T_RS] = self.seq_data['tpose_pc'][index]   
        if self.kb[T_SK]:
            out[T_SK] = self.seq_data['tpose_sk'][index]
        if self.kb[PCD_SMPL]:
            out[PCD_SMPL] = self.seq_data['pc_smpl'][index][start:end][sampling]
        if self.kb[RM_PCD]:
            out[RM_PCD] = self.seq_data['remeshed_pointcloud'][index][start:end][sampling]                                              
        if self.kb[RM_T]:
            out[RM_T] = self.seq_data['remeshed_tpose'][index]    
        if self.kb[POSE]:
            out[POSE] = self.seq_data['pose'][index][start:end][sampling] 
        if self.kb[TRANS]:
            out[TRANS] = self.seq_data['trans'][index][start:end][sampling] 
        
        return [out[k] for k in self.keys]
    
    def get_static(self,index):
        
        seq_idx = self.data["seq_idx"][index]
        frame_idx = self.data["frame_idx"][index]
     
        out = [None for i in range(TRANS+1)]
        
        if self.kb[PCD]:
            out[PCD] = self.seq_data['pointcloud'][seq_idx][frame_idx]  
        if self.kb[SK] : 
            out[SK] = self.seq_data['skeleton'][seq_idx][frame_idx]               
        if self.kb[SK_REG]:
            out[SK_REG] = self.seq_data['skeleton_regressed'][seq_idx][frame_idx]       
        if self.kb[T_SK_REG]:
            out[T_SK_REG] = self.seq_data['tpose_regressed'][seq_idx]
        if self.kb[T_PC]:               
            out[T_PC] = self.seq_data['tpose_pc'][seq_idx]
        if self.kb[T_RS]:
            out[T_RS] = self.seq_data['tpose_pc'][seq_idx]
        if self.kb[T_SK]:
            out[T_SK] = self.seq_data['tpose_sk'][seq_idx]
        if self.kb[PCD_SMPL]:
            out[PCD_SMPL] =  self.seq_data['pc_smpl'][seq_idx][frame_idx]
        if self.kb[RM_PCD]:
            out[RM_PCD] = self.seq_data['remeshed_pointcloud'][seq_idx][frame_idx]
        if self.kb[RM_T]:
            out[RM_T] = self.seq_data['remeshed_tpose'][seq_idx]  
        if self.kb[POSE]:
            out[POSE] = self.seq_data['pose'][seq_idx][frame_idx]
        if self.kb[TRANS]:
            out[TRANS] = self.seq_data['trans'][seq_idx][frame_idx] 
        if self.shuffle_point:
            random_sample = np.random.choice(6890,size=6890,replace=False)
            out[RM_PCD] = out[RM_PCD][random_sample]
            out[RM_T] = out[RM_T][random_sample]

        return [out[k]for k in self.keys]  
           
    def __getitem__(self,index):
        
        if self.sequential:
            return self.get_sequential(index)
        else:
            return self.get_static(index)
  
    def __len__(self):
        if self.sequential:
            return len(self.seq_data["names"])
        else:
            return len(self.data["seq_idx"])

