import torch
import torch.utils.data
import os
import torch
from torch.utils.data import Dataset
from dataset.resampler import Resampler
import trimesh
import re
def as_mesh(scene_or_mesh):
    """
    Convert a possible scene to a mesh.

    If conversion occurs, the returned mesh has only vertex and face data.
    """
    if isinstance(scene_or_mesh, trimesh.Scene):
        if len(scene_or_mesh.geometry) == 0:
            mesh = None  # empty scene
        else:
            # we lose texture information here
            mesh = trimesh.util.concatenate(
                tuple(trimesh.Trimesh(vertices=g.vertices, faces=g.faces)
                    for g in scene_or_mesh.geometry.values()))
    else:
        assert(isinstance(scene_or_mesh, trimesh.Trimesh))
        mesh = scene_or_mesh
    
    return mesh

class EvaluationDataset(Dataset):
    def __init__(self,data_path,resampling=["remeshed"],npoints=6890):
        
        self.seq_data = dict()
        self.tpose_data = dict()
        self.nsubjects = 0
        self.nactions = 0
        self.npoints=npoints
        self.length = 0
        self.remesher = Resampler(fc_path="/morpheo-nas/datarim/fc.pt",
                        coords_path="/morpheo-nas/datarim/bary_coords.pt",
                        indices_path="/morpheo-nas/datarim/SMPLface_indices.pt",
                        faces_rm_path="/morpheo-nas/datarim/fixed_rm_faces.pt")
        
        self.remeshed, self.uniform, self.basic="remeshed"in resampling, "uniform" in resampling, "basic" in resampling
        for r,d,f in os.walk(data_path):
           
            if len(f) > 30:
                
                subject,action = r.replace(data_path,"").split(os.sep)[1:]

                if subject not in self.seq_data.keys():
                    self.seq_data[subject] =dict()
                    
                if action not in self.seq_data[subject].keys():
                    self.seq_data[subject][action] =dict()
                f = [frame for frame in f if ".obj" in frame]
                self.seq_data[subject][action] = [as_mesh(trimesh.load(os.path.join(r,frame),process=False)) for frame in sorted(f, key=lambda x:float(re.findall("(\d+)",x)[0]))]
                
                self.length +=1
            elif len(f)>0:
              
                subject= r.replace(data_path,"").split(os.sep)[1]
                self.tpose_data[subject] = as_mesh(trimesh.load(os.path.join(r,"tpose.obj")))
                
                    
        self.nsubjects = len(list(self.seq_data.keys()))
        self.nactions = len(list(self.seq_data[list(self.seq_data.keys())[0]].keys()))
       
    def __getitem__(self, index):
        
        
        sid = index// self.nactions
        aid = index % self.nactions
       
        sublabel = list(self.seq_data.keys())[sid]
        actlabel = list(self.seq_data[sublabel].keys())[aid]

        out = []
        out_tpose = []
        if self.remeshed: 
            out_tpose.append(self.remesher.resample(torch.from_numpy(self.tpose_data[sublabel].vertices)))
            out.append(torch.stack(
                        [self.remesher.resample(torch.from_numpy(frame.vertices)) for frame in  self.seq_data[sublabel][actlabel]],dim=0))
        if self.uniform:
            out_tpose.append(torch.from_numpy(trimesh.Trimesh.sample(self.tpose_data[sublabel],self.npoints)))
            out.append(torch.stack(
                        [torch.from_numpy(trimesh.Trimesh.sample(frame,self.npoints)) for frame in  self.seq_data[sublabel][actlabel]],dim=0))
            
        if self.basic:
            out_tpose.append(torch.from_numpy(self.tpose_data[sublabel].vertices))
            out.append(torch.stack(
                        [torch.from_numpy(frame.vertices) for frame in  self.seq_data[sublabel][actlabel]],dim=0))
          
        return out,out_tpose, sublabel,actlabel
    
    
    def __len__(self):
        return self.length

