import torch
import numpy as np
import trimesh
from human_body_prior.body_model.body_model import BodyModel
import os 
import multiprocessing as mp
import time
import argparse
from tqdm import tqdm
import psutil
import math
from resampler import Resampler



RESTRICTION = {"ACCAD","SSM","SFU","CMU"} # Select the AMASS subset we are interested in
MAX_RAM = int(62-psutil.virtual_memory()[2]*62/100) # max ram to prevent multiprocessing crashes (Replace 62 with your max ram capacity when no program is running)
FPS = 30 # Target FPS , we want 30fps AMASS sequences as the retargetting module operates at 30FPS


def init_ram(ram):
    global _available_ram 
    _available_ram = ram
    
def process_motion(out_experiment_path,in_experiment_path,motion,body_model,remesher):
    """_summary_

    Args:
        out_experiment_path (str): out path for storing motion data files
        in_experiment_path (str): input amass motion path
        motion (str): motion name.npz
        body_model (BodyModel): the neutral smplh body model
        remesher (Resampler): resampler used to remesh pointclouds

    """
    
    motion_path = os.path.join(in_experiment_path,motion)
    motion_name = motion[:-4]
    out_motion_path = os.path.join(out_experiment_path,motion_name)
   
    #check motion file validity 
    if not '.npz' in motion :\
        #or os.path.exists(os.path.join(out_motion_path,"nof.pt")): 
        return 0
    
    amass_motion = dict(np.load(motion_path))
    
    # check that all required keys are available in the npz data
    if not {'poses','betas','trans','dmpls'}.issubset(
        set(list(amass_motion.keys()))):
        return 0
    
    if not os.path.exists(out_motion_path): 
        os.mkdir(out_motion_path)        
    elif os.path.exists(os.path.join(out_motion_path,"nof.pt")):# comment if willing to reprocess what has already been processed 
        return
    
    # find temporal sampling to convert to 30FPS
    fps = amass_motion['mocap_framerate']
    number_of_frames = amass_motion['poses'].shape[0]
    fps_ratio = FPS/fps
    new_number_of_frames = math.ceil(number_of_frames*fps_ratio)
    sampling = torch.linspace(0,number_of_frames-fps_ratio,new_number_of_frames).long() 
    number_of_frames = new_number_of_frames
    fps = FPS
    
    # resample to 30FPS
    amass_motion['poses'] = amass_motion['poses'][sampling]
    amass_motion['trans'] = amass_motion['trans'][sampling]
    amass_motion['dmpls'] = amass_motion['dmpls'][sampling]
    
    betas = torch.from_numpy(amass_motion['betas'])[None].repeat(number_of_frames,1).type(torch.float)
    
    # estimate needed ram for SMPL calculation which is costly
    # if too much RAM is needed (too many frames, split in smaller batches)
    needed_ram = int(
        (number_of_frames+1)*6890*3*(body_model.posedirs.shape[0])*4*1e-9)+1
    n_batches = int(needed_ram//MAX_RAM)+1
    frames_per_batch = int(number_of_frames//n_batches)+1
    needed_ram = int(needed_ram*(frames_per_batch/number_of_frames))
    
    global _available_ram
    
    resl = []
    for i in range(n_batches):
        while _available_ram.value<needed_ram:
            time.sleep(1) # wait for ram to get freed
        
        # reserve estimated RAM for a batch 
        with _available_ram.get_lock():
            _available_ram.value = _available_ram.value-needed_ram
           
        resl.append(body_model(**{
            'pose_body': torch.from_numpy(
                amass_motion['poses'][i*frames_per_batch:(i+1)*frames_per_batch,3:66]).type(torch.float),
            'root_orient': torch.from_numpy(
                amass_motion['poses'][i*frames_per_batch:(i+1)*frames_per_batch,:3]).type(torch.float),
            'betas': betas[i*frames_per_batch:(i+1)*frames_per_batch],
            'trans': torch.from_numpy(
                amass_motion['trans'][i*frames_per_batch:(i+1)*frames_per_batch]).type(torch.float),
            'dmpls': torch.from_numpy(
                amass_motion['dmpls'][i*frames_per_batch:(i+1)*frames_per_batch]).type(torch.float)}))
    
        # release RAM 
        with _available_ram.get_lock():
            _available_ram.value = _available_ram.value+needed_ram

    
    tpose_res = body_model(**{
        'pose_body': torch.zeros_like(
            torch.from_numpy(
                amass_motion['poses'][0:1,3:66])).type(torch.float),
        'root_orient': torch.zeros_like(
            torch.from_numpy(
                amass_motion['poses'][0:1,:3]).type(torch.float)),
        'betas': betas[0:1],
        'trans': torch.zeros_like(
            torch.from_numpy(
                amass_motion['trans'][0:1]).type(torch.float)),
        'dmpls': torch.from_numpy(
            amass_motion['dmpls'][0:1]).type(torch.float)})
    
    mesh = trimesh.Trimesh()
    faces = tpose_res.f
    joints = torch.cat([r.Jtr for r in resl],dim=0)
    resl = torch.cat([r.v for r in resl],dim=0)
    
    N=6890 # numbe rof points to resampled from SMPL meshes
    pointclouds = resl
    resampled_pointclouds = torch.zeros((number_of_frames,N,3),
                                        dtype=torch.float)
    remeshed_pointclouds = torch.zeros((number_of_frames,N,3),
                                        dtype=torch.float)
    tpose = tpose_res.v[0].type(torch.float)
    mesh.vertices = tpose.numpy()
    mesh.faces = faces.numpy()
    resampled_tpose = torch.from_numpy(
            trimesh.Trimesh.sample(mesh,6890)).type(torch.float)

    
    # Resampling the pointclouds
    for i in range(number_of_frames):
        mesh.vertices = pointclouds[i].numpy()
        mesh.faces = faces.numpy()
        resampled_pointclouds[i,...] = torch.from_numpy(
            trimesh.Trimesh.sample(mesh,6890)).type(torch.float)
    
    # I store all the motion sequence as one big pointcloud 
    # of number_of_framesx6890 points, same for skeletons
    pose_path = os.path.join(out_motion_path,"pose30fps.npz")
    smpl_pointcloud_path = os.path.join(out_motion_path,"smpl_pointcloud.pt")
    remeshed_pointcloud_path = os.path.join(out_motion_path,"remeshed_pointcloud.pt") 
    remeshed_tpose_path = os.path.join(out_motion_path,"remeshed_tpose.pt") 
    resampled_pointcloud_path = os.path.join(out_motion_path,"pointcloud.pt")
    tpose_pointcloud_path = os.path.join(out_motion_path,"tpose.pt")
    resampled_tpose_pointcloud_path = os.path.join(out_motion_path,"tpose_rs.pt")
    skeleton_path = os.path.join(out_motion_path,"skeleton.pt")
    tpose_skeleton_path = os.path.join(out_motion_path,"tpose_skeleton.pt")

    smpl_storage = torch.FloatStorage.from_file(
        smpl_pointcloud_path, True, size=int(torch.numel(pointclouds)))
    torch.FloatTensor(smpl_storage).copy_(pointclouds.reshape(-1))
    
    for i in range(number_of_frames):
        remeshed_pointclouds[i] = remesher.resample(pointclouds[i])
    remeshed_smpl_storage = torch.FloatStorage.from_file(
        remeshed_pointcloud_path, True, size=int(torch.numel(remeshed_pointclouds)))
    torch.FloatTensor(remeshed_smpl_storage).copy_(remeshed_pointclouds.reshape(-1))

    rs_storage = torch.FloatStorage.from_file(
        resampled_pointcloud_path, True, size=int(torch.numel(resampled_pointclouds)))
    torch.FloatTensor(rs_storage).copy_(resampled_pointclouds.reshape(-1))

    tpose_storage = torch.FloatStorage.from_file(
        tpose_pointcloud_path, True, size=int(torch.numel(tpose)))
    torch.FloatTensor(tpose_storage).copy_(tpose.reshape(-1))
    
    remeshed_tpose = remesher.resample(tpose-torch.mean(tpose,dim=-2,keepdim=True))
    remeshed_tpose_storage = torch.FloatStorage.from_file(
        remeshed_tpose_path, True, size=int(torch.numel(remeshed_tpose)))
    torch.FloatTensor(remeshed_tpose_storage).copy_(remeshed_tpose.reshape(-1))
    
    resampled_tpose_storage = torch.FloatStorage.from_file(
        resampled_tpose_pointcloud_path, True, size=int(torch.numel(resampled_tpose)))
    torch.FloatTensor(resampled_tpose_storage).copy_(resampled_tpose.reshape(-1))
    
    skeleton_storage = torch.FloatStorage.from_file(
        skeleton_path, True, size=int(torch.numel(joints)))
    torch.FloatTensor(skeleton_storage).copy_(joints.reshape(-1))
    
    tpose_skeleton_storage = torch.FloatStorage.from_file(
        tpose_skeleton_path, True, size=int(torch.numel(tpose_res.Jtr[0])))
    torch.FloatTensor(tpose_skeleton_storage).copy_(tpose_res.Jtr[0].reshape(-1))
    
    #sequence metadata
    fps_path =  os.path.join(out_motion_path,"fps.pt")
    nof_path =  os.path.join(out_motion_path,"nof.pt")
    
    np.savez(pose_path,**amass_motion)
    torch.save(fps,fps_path)
    torch.save(number_of_frames,nof_path)
       
    return 0
   
def main(args):
    
    print("Number of processors: ", mp.cpu_count())
   
    datasets={
        datasets  for datasets in os.listdir(args.amass_path) if not datasets.startswith('.')}
    
    if RESTRICTION:
        datasets = list(datasets.intersection(RESTRICTION))
    else:
        datasets = list(datasets)
    datasets.sort()
    print("%d datasets detected:"%len(datasets))
    print(datasets)
    
    bm_fname = args.bm_path
    bm = BodyModel(bm_fname= bm_fname,num_betas=16).cpu()
    resampler = Resampler(fc_path=args.fc_path,
                        coords_path=args.coords_path,
                        indices_path=args.face_path)
    tps1 = time.time()
    
    if not os.path.exists(args.out_path):
        os.mkdir(args.out_path)
    
    for dataset in datasets:
        dataset_path = os.path.join(args.amass_path,dataset)+os.sep
        print("Processing %s"%dataset)
        experiments=[experiment  for experiment in os.listdir(dataset_path) if not experiment.startswith('.')]
        experiments.sort()
        if not os.path.exists(os.path.join(args.out_path,dataset)):
            os.mkdir(os.path.join(args.out_path,dataset))
            
        for experiment in tqdm(experiments):
            if not os.path.exists(os.path.join(args.out_path,dataset,experiment)):
                os.mkdir(os.path.join(args.out_path,dataset,experiment))
            
            experiment_path = dataset_path + experiment +'/' 
            motions=[motion  for motion in os.listdir(experiment_path) if not motion.startswith('.')]
            motions.sort()

            global _available_ram 
            _available_ram = mp.Value(
                'i',
                MAX_RAM)
 
            out_experiment_path = os.path.join(args.out_path,dataset,experiment)
            in_experiment_path = os.path.join(args.amass_path,dataset,experiment)
            
            if args.mp:
                pool = mp.Pool(mp.cpu_count(),initializer=init_ram,initargs=(_available_ram,))
                
                [pool.apply_async(process_motion, 
                                args=(out_experiment_path,
                                        in_experiment_path, 
                                        motion,bm,resampler)) for motion in motions]
                pool.close()
                pool.join()
            else:
                [process_motion(out_experiment_path,in_experiment_path, motion,bm,resampler)for motion in motions]
                  
    tps2 = time.time()
    t=tps2 -tps1
    print("Data processing done in %fs"%t )
   
    
if __name__ == "__main__":
    mp.set_start_method('spawn', force=True) 
    parser = argparse.ArgumentParser()
    parser.add_argument("--bm_path",
                        help="Body model path", default="./smplh/neutral/model.npz")

    parser.add_argument("--amass_path",
                        help="path to folder containing amass data", default="./amass/")
    
    parser.add_argument("--out_path","-o",
                        help="output folder", default="./amass_processed/")
    parser.add_argument("--mp",
                        help="multiprocessing", action="store_true")
    # resampler paths
    parser.add_argument("--fc_path",
                        help="", default="./fixed_resampling/fc.pt")
    parser.add_argument("--coords_path",
                        help="", default="./fixed_resampling/bary_coords.pt")
    parser.add_argument("--face_path",
                        help="", default="./fixed_resampling/SMPLface_indices.pt")
    
    args = parser.parse_args()
    with torch.no_grad():
        main(args)
    
