import torch.utils.data
import torch
import torch.optim as optim
import time
from dataset.amass_dataset import *
from models.pointcloud_from_skeleton import *
from models.skeleton_from_pointcloud import PointNetRegression
import argparse
import json
"""### Dataset"""

def train(config):
    data_path= config["data_path"]
    batch_size= config["batch_size"]
    outf = config["out_folder"]

    keys=[POSE,RM_T,RM_PCD,T_SK]

    if config["part1_tsk"]:
        keys[3] = T_SK_REG
        
    train_dataset = AmassDataset(data_path,
        split ='train',
        sequential=False,
        fps=None,
        keys=keys,
        shuffle_point= True)
    test_dataset = AmassDataset(data_path,
        split ='test',
        sequential=False,
        fps=None,
        keys=keys,
        shuffle_point= True)

    traindataloader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size, shuffle=True, num_workers=16)
    testdataloader = torch.utils.data.DataLoader(test_dataset, batch_size=batch_size, shuffle=True, num_workers=16)

    num_batch =len(traindataloader) 
    num_batch_test = len(testdataloader) 
    num_joints = config["num_joints"]
    regressor = PointNetRegression(output_size=3, k=num_joints)
    if config["initialization_p1"] != "":
        regressor.load_state_dict(torch.load(config["initialization_p1"]))
    regressor.eval()
    
    
    kintree_table = torch.tensor([
        [4294967295,0,0,0,1,2,3,4,5,6,7,8,9,9,9,12,13,14,16,17,18,19],
        [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21]]).cuda()
    model = RiggingRenderer(skeleton_regressor=regressor,num_joints=num_joints,kintree_table=kintree_table)
    model.cuda()

    lrate=config["lr"]
    optimizer_G = optim.Adam(model.rigging_field.parameters(), lr=lrate)

    train_losses = []
    test_losses=[]
    
    if config["initialization"] != "":  
        model.load_state_dict(torch.load(config["initialization"]),strict=False)

    for epoch in range(config["num_epochs"]):

        s,s_rec=0,0
        stest = 0
        
        a=time.time()
        
        for j,data in enumerate(traindataloader,0):
            
            optimizer_G.zero_grad()
            
            rotation_target_reposed, pointcloud_target, ground_truth,tposes=data
            rotation_target_reposed = rotation_target_reposed.cuda()
            pointcloud_target= pointcloud_target.cuda()
            ground_truth = ground_truth.cuda()
            
            pointsReconstructed = model(rotation_target_reposed.float(),pointcloud_target.float(),tposes=tposes.cuda()) 
        
            retar_loss= torch.mean((pointsReconstructed - ground_truth)**2)
            if(epoch<1):
                init_loss = torch.mean((model.pseudo_rigging-model.nn_rigging)**2)
                retar_loss =init_loss
            elif(epoch<2):
                init_loss = torch.mean((model.pseudo_rigging-model.nn_rigging)**2)
                retar_loss +=init_loss
            retar_loss.backward()
            optimizer_G.step()
            s = s+retar_loss.detach()
            s_rec=s_rec+retar_loss.detach()
            
            print('[%d,%.2f] train loss: %f retar loss: %f' % (epoch,j/num_batch, s/(j+1),s_rec/(j+1)),end="\r")
        for j,data in enumerate(testdataloader,0):
            
            rotation_target_reposed, pointcloud_target, ground_truth,tposes=data
        
            rotation_target_reposed = rotation_target_reposed.cuda()
            pointcloud_target= pointcloud_target.cuda()
            ground_truth = ground_truth.cuda()
            
            pointsReconstructed = model(rotation_target_reposed.float(),pointcloud_target.float(),tposes=tposes.cuda()) 
            retar_loss= torch.mean((pointsReconstructed - ground_truth)**2)
            if(epoch<10):
                init_loss = torch.mean((model.pseudo_rigging-model.nn_rigging)**2)
                retar_loss += 0.1*init_loss
            stest = stest+retar_loss.detach()

        print("Epoch took %f s"%(time.time()-a))
        a = time.time()
        s= s/num_batch
        stest = stest/num_batch_test
        train_losses.append(s.detach().cpu())
        test_losses.append(stest.detach().cpu())

        print('[%d] train loss: %f' % (epoch, s)+'\n')
        print('[%d] test loss: %f' % (epoch, stest)+'\n')

        if(epoch%2==0):
            torch.save(model.state_dict(), '%s/rend_%d.pth' % (outf, epoch))
            torch.save({"train":train_losses,"test":test_losses},'%s/losses.pt' % (outf, ))

if __name__ == "__main__":   
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", type=str,
                        help="Configuration file for the skinning module, defaults to config_part3.json", default="configurations/config_part3.json")
    
    args = parser.parse_args()
    train(json.load(open(args.config)))
    