from gettext import translation
import torch
from dataset.amass_dataset import *
import torch.optim as optim
from models.skeleton_rnn import SkeletonReposing,Discriminator
import numpy as np
import random
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation,PillowWriter
from tqdm import tqdm
import time
from human_body_prior.tools.omni_tools import makepath
import argparse 
import json
from utils.representation import *
import os

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
title = ax.set_title('3D Test')
ln, = ax.plot([],[],[], linestyle="", marker="o",ms=0.5)
lngt, = ax.plot([],[],[], linestyle="", marker="o",ms=0.5,color="red")
lnret, = ax.plot([],[],[], linestyle="", marker="o",ms=0.5,color="green")
writergif = PillowWriter(fps=30) 

def init():
    ax.set_xlim(-1.5, 1.5)
    ax.set_ylim(-1.5, 1.5)
    ax.set_zlim(-1.5, 1.5)
    return ln,lngt,lnret,

def update(data):
    ln.set_data (data[:,0,0], data[:,0,1])
    ln.set_3d_properties(data[:,0,2])
    lngt.set_data (data[:,1,0], data[:,1,1])
    lngt.set_3d_properties(data[:,1,2])
    lnret.set_data (data[:,2,0], data[:,2,1])
    lnret.set_3d_properties(data[:,2,2])

    return ln,lngt,lnret,

def get_unnorm(skeletons,translations,rotations):
    
    bs,nof = skeletons.shape[:2]
    skeletons_nt = torch.bmm(
            convertPose(rotations.clone(),QUAT,MAT)[:,:,None].repeat(1,1,skeletons.shape[2],1).reshape(-1,3,3).transpose(-1,-2), 
            skeletons.reshape(-1,3,1)
            ).reshape(bs,nof,-1,3)
    
    translations  = torch.bmm(
        convertPose(rotations.clone(),QUAT,MAT)[:,:,None].repeat(1,1,translations.shape[2],1).reshape(-1,3,3).transpose(-1,-2), 
        translations.reshape(-1,3,1)
        ).reshape(bs,nof,-1,3)
    
   
    #translation = torch.cumsum(velocities,dim=1)
    
    return skeletons_nt + translations
def train(config):
    
    nepoch = config["num_epochs"]
    outf = config["out_folder"]
    makepath(outf)
    makepath(os.path.join(outf,"vids"))
    makepath(os.path.join(outf,"model"))
    data_path = config["data_path"]
    batchSize = config["batch_size"]
    use_part1_skeletons = config["part1_sk"]
    use_part1_tposes_skeletons = config["part1_tsk"]
    
    keys = []
    if use_part1_skeletons:
        keys.append(SK_REG)
    else:
        keys.append(SK)
    
    if use_part1_tposes_skeletons:
        keys.append(T_SK_REG)
    else:
        keys.append(T_SK)
    keys.append(POSE) 
    train_dataset = AmassDataset(
        data_path= data_path,
        split='train',
        sequential=True,
        fps=30,
        num_frames=config["num_frames"],
        keys=keys
        )
    test_dataset = AmassDataset(
        data_path= data_path,
        split='test',
        sequential=True,
        fps=30,
        num_frames=120,
        keys=keys)
    
    workers= 16
    traindataloader = torch.utils.data.DataLoader(
        train_dataset,
        batch_size=batchSize,
        shuffle=True,
        num_workers= workers)

    testdataloader = torch.utils.data.DataLoader(
        test_dataset,
        batch_size=batchSize,
        shuffle=True,
        num_workers=workers)
    
    num_batch = len(traindataloader) / batchSize
    num_batch_test = len(testdataloader) / batchSize

    parents = np.array([
      -1, 0, 0,0,1,2,3,4,5,6,7,8,9,9,9,12,13,14,16,17,18,19])
    
    skrepose = SkeletonReposing(parents).cuda()
    
    if config["initialization"] != "":
        skrepose.load_state_dict(torch.load(config["initialization"]),strict=False)
        init_lim=-1
    else:
        init_lim=10
    gen_optimizer = optim.Adam(skrepose.parameters(), lr=config["lr"], betas=(0.9, 0.999))
    scheduler = optim.lr_scheduler.ReduceLROnPlateau(gen_optimizer, patience=50,cooldown=50,factor=0.1,min_lr=1e-6)
    discriminator = Discriminator(len(parents)).cuda()
    disc_optimizer = optim.Adam(discriminator.parameters(), lr=config["lr"], betas=(0.9, 0.999))
   
   
    train_losses = []
    test_losses=[]
    disc_losses = []
    gen_losses = []
    best_loss = 10000
    # with torch.autograd.detect_anomaly():
    for epoch in range(nepoch):
        s=0
        stest = 0
        dl = 0
        gl=0
        a = time.time()
        for  (skeletons,tposes,poses) in tqdm(traindataloader):
        
            skeletons_nt,translations,tposes,poses_quat,norm_rot_input = skrepose.preprocess(skeletons.cuda(),tposes.cuda(),poses.cuda())
        
            mix_indexes = list(range(skeletons.shape[0]))
            random.shuffle(mix_indexes)
            
            pos_retar,rot_retar,trans_retar =skrepose.forward(skeletons_nt.cuda(),translations.cuda(),tposes[mix_indexes].cuda()) 
            pos_cycon,rot_cycon,trans_cycon  = skrepose.forward(pos_retar,trans_retar,tposes) 
            
            
            loss,gen_loss,disc_loss = skrepose.compute_loss(
                {"pos_input":skeletons_nt,
                "tpos_input":tposes,
                "trans_input":translations, 
                "rot_input":poses_quat,
                "pos_retar":pos_retar,
                "rot_retar": rot_retar,
                "trans_retar":trans_retar,
                "pos_cycon":pos_cycon,
                "rot_cycon":rot_cycon,   
                "trans_cycon":trans_cycon,             
                "mix_indexes":mix_indexes,
                "twist_threshold":config["twist_threshold"],
                "twist_weight":config["twist_weight"],
                "smooth_weight":config["smooth_weight"],
                "rot_weight":config["rot_weight"],
                "beta":config["beta"],
                "init":epoch<init_lim},discriminator=discriminator)
            
            gen_optimizer.zero_grad()
            
            if epoch>init_lim and discriminator is not None and discriminator.fake_score > discriminator.margin: 
                disc_optimizer.zero_grad()
                disc_loss.backward(retain_graph=True)
                disc_optimizer.step()
        
            loss.backward()     
            gen_optimizer.step()
            
            if epoch>init_lim and discriminator is not None:
                discriminator.fake_score = (0.99 * discriminator.fake_score + 0.01 * discriminator.cur_score).detach()
            dl=dl+disc_loss.detach()
            gl=gl+gen_loss.detach()
            s=s+loss-gen_loss
            
        for (skeletons,tposes,poses) in  tqdm(testdataloader):
            
            skeletons_nt,translations,tposes,poses_quat,norm_rot_input = skrepose.preprocess(skeletons.cuda(),tposes.cuda(),poses.cuda())

            mix_indexes = list(range(skeletons.shape[0]))
            random.shuffle(mix_indexes)
            
            
            pos_retar,rot_retar,trans_retar =skrepose.forward(skeletons_nt,translations,tposes[mix_indexes]) 
            pos_cycon,rot_cycon,trans_cycon  = skrepose.forward(pos_retar,trans_retar,tposes) 
             
            loss,gen_loss,disc_loss = skrepose.compute_loss(
                {"pos_input":skeletons_nt,
                "tpos_input":tposes,
                "trans_input":translations, 
                "rot_input":poses_quat,
                "pos_retar":pos_retar,
                "rot_retar": rot_retar,
                "trans_retar":trans_retar,
                "pos_cycon":pos_cycon,
                "rot_cycon":rot_cycon,   
                "trans_cycon":trans_cycon,             
                "mix_indexes":mix_indexes,
                "twist_threshold":config["twist_threshold"],
                "rot_weight":config["rot_weight"],
                "twist_weight":config["twist_weight"],
                "smooth_weight":config["smooth_weight"],
                "beta":config["beta"],
                "init":epoch<init_lim},discriminator=discriminator)
            stest = stest+loss-gen_loss
        
        
        if(epoch%10==0 and epoch>init_lim):
            
            sk_in = get_unnorm(skeletons_nt,translations,norm_rot_input)
            sk_rec=  get_unnorm(pos_cycon,trans_cycon,norm_rot_input)
            sk_retar= get_unnorm(pos_retar,trans_retar,norm_rot_input)
            ani = FuncAnimation(
                fig, 
                update, 
                frames=torch.cat((sk_in[0,:,:,None,:],
                                sk_rec[0,:,:,None,:],
                                sk_retar[0,:,:,None,:]),
                                dim=-2).detach().cpu().numpy(),
                init_func=init, 
                blit=True)
            
            ani.save(
                os.path.join(outf,"vids",'res%d.gif'%epoch),writer=writergif)
            
                
        print("Epoch took %f s"%(time.time()-a))
        a = time.time()
        s= s/num_batch
        dl = dl/num_batch
        gl = gl/num_batch
        stest = stest/num_batch_test
        train_losses.append(s.detach().cpu())
        test_losses.append(stest.detach().cpu())
        gen_losses.append(gl.detach().cpu())
        disc_losses.append(dl.detach().cpu())
        print('[%d] train loss: %f' % (epoch, s))
        print('[%d] test loss: %f' % (epoch, stest))
        print('[%d] disc loss: %f' % (epoch, dl))
        print('[%d] gen loss: %f' % (epoch, gl))
        scheduler.step(s)
        if(epoch%50==0):
            torch.save(skrepose.state_dict(), '%s/model/skrep_%d.pt' % (outf, epoch))
            torch.save(discriminator.state_dict(), '%s/model/disc_%d.pt' % (outf, epoch))
            torch.save({"train":train_losses,"test":test_losses,"gen":gen_losses,"disc":disc_losses},'%s/losses.pt' % (outf, ))

        if(best_loss > stest and epoch>init_lim):
            print("Saving")
            best_loss=stest
            torch.save(skrepose.state_dict(), '%s/model/skrep.pt' % outf)
            torch.save(discriminator.state_dict(), '%s/model/disc.pt' % outf)
            
if __name__ == "__main__":   
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", type=str,
                    help="Configuration file for the skeleton rnn module, defaults to config_part2.json", default="configurations/config_part2.json")

    args = parser.parse_args()
    train(json.load(open(args.config)))
