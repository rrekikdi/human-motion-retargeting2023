def smpl2amass(points):
   
    points = points[...,[0,2,1]]
    points[...,1] = -points[...,1]
    
    return points


def amass2smpl(points):
    points = points[...,[0,2,1]]
    points[...,2] = -points[...,2]
    
    return points 