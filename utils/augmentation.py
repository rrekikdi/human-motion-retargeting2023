import torch
import numpy as np

## Credit of the code to https://www.blopig.com/blog/2021/08/uniformly-sampled-3d-rotation-matrices/ 
def generate_random_z_axis_rotation(bs):
    """Generate random rotation matrix about the z axis."""
    R = torch.eye(3)[None].repeat(bs,1,1)
    x1 = torch.rand(bs)
    R[:,0, 0] = R[:,1, 1] = torch.cos(2 * np.pi * x1)
    R[:,0, 1] = -torch.sin(2 * np.pi * x1)
    R[:,1, 0] = torch.sin(2 * np.pi * x1)
    
    x2 = 2 * np.pi * torch.rand(bs)
    x3 = torch.rand(bs)
    return R,x2,x3
    
def apply_random_rotation(x,R,x2,x3,mean_coord=None):
    """Apply a random rotation in 3D, with a distribution uniform over the
    sphere.
    Arguments:
        x: vector or set of vectors with dimension (n, 3), where n is the
            number of vectors
    Returns:
        Array of shape (n, 3) containing the randomly rotated vectors of x,
        about the mean coordinate of x.
    Algorithm taken from "Fast Random Rotation Matrices" (James Avro, 1992):
    https://doi.org/10.1016/B978-0-08-050755-2.50034-8
    """
    
    # There are two random variables in [0, 1) here (naming is same as paper)
    bs = x.shape[0]
    
    # Rotation of all points around x axis using matrix
    v = torch.stack([
        torch.cos(x2) * torch.sqrt(x3),
        torch.sin(x2) * torch.sqrt(x3),
        torch.sqrt(1 - x3)
    ],dim=1)
    outer = torch.einsum('bi,bj->bij', (v, v))
   
    H = torch.eye(3,device=x.device)[None].repeat(bs,1,1) - (2 * outer)
    M = -torch.bmm(H,R)
    x = x.reshape((bs,-1, 3))
    if mean_coord is None:
        mean_coord = torch.mean(x, dim=1,keepdim=True)
    return torch.bmm((x - mean_coord) , M) + torch.bmm(mean_coord , M)

