from calendar import c
import torch
from dataset.amass_dataset import *
import torch.optim as optim
import torch.nn.functional as F
from models.skeleton_rnn import SkeletonReposing,Discriminator
import numpy as np
import random
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation,PillowWriter
import pytorch3d.transforms as T
from tqdm import tqdm
import time
from human_body_prior.tools.omni_tools import makepath
import argparse 
import json
from models.pointcloud_from_skeleton import *
from models.skeleton_from_pointcloud import PointNetRegression
from utils.representation import *
def main(args):
    
    outf = "./out/results/"
    makepath(outf)
   
    data_path = "/amass_processed"
    
    test_dataset = AmassDataset(data_path= data_path,
                                split='test',
                                sequential=True,
                                fps=30,
                                num_frames=120,
                                keys=[SK_REG,T_SK_REG,T_SK,RM_T,POSE])
   

    testdataloader = torch.utils.data.DataLoader(
        test_dataset,
        batch_size=2,
        shuffle=False,
        num_workers=8)


    parents = np.array([
      -1, 0, 0,0,1,2,3,4,5,6,7,8,9,9,9,12,13,14,16,17,18,19])
    kintree_table = torch.tensor([
            [4294967295,0,0,0,1,2,3,4,5,6,7,8,9,9,9,12,13,14,16,17,18,19],
            [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21]]).cuda()
    
    skregressor  = PointNetRegression(output_size=3, k=len(parents))
    skregressor.load_state_dict(torch.load("out/1stpart/reg_model.pth"))
    skregressor.cuda().eval()
    skrepose = SkeletonReposing(parents).cpu()
    skrepose.load_state_dict(torch.load("out/2ndpart/model/skrep.pt"))
    skrepose.cpu().eval()
    skinning_model = RiggingRenderer(skeleton_regressor=skregressor,num_joints=len(parents),kintree_table=kintree_table)
    skinning_model.load_state_dict(torch.load("out/3rdpart/rend.pth"))
    skinning_model.cpu().eval()
    
    
    for  j,(skeletons,tposes,tpose_gt,tpcd,pose) in tqdm(enumerate(testdataloader)):
        
        skeletons_wt = skeletons[0:1,:,:len(parents),:].cpu()
        
        
        tposes = tposes[1:,:len(parents),:].cpu()
        T0 =   tposes[:,0:1,:].clone()
        tposes = tposes-T0
        
     
        tpose_gt = tpose_gt[1:,:len(parents),:].cpu()
       
        tpcd = tpcd[1:].cpu()-T0
        #BSxNoFxNoJx3
        translation =  (skeletons_wt[:,:,0:1,:])
        translation = translation- translation[:,0:1]
        
        skeletons_nt = skeletons_wt - skeletons_wt[:,:,0:1,:].clone()
        
        pos_retar,rot_retar,trans_retar =skrepose.forward(skeletons_nt,translation,tposes) 
        rot_retar_aa = convertPose(rot_retar,QUAT,AA)
       
       
        pose_gt =pose[0:1,:,:22].cpu()
 
        final_output = skinning_model.forward(rot_retar_aa[0].float(),tpcd.repeat(120,1,1),tposes=tposes.repeat(120,1,1))
        final_output_gt = skinning_model.forward(pose_gt[0].float(),tpcd.repeat(120,1,1),tposes=tposes.repeat(120,1,1))
        final_output_gt = final_output_gt + trans_retar[0]
        final_output = final_output + trans_retar[0]
        
        seq_path = os.path.join(outf,"seq%d"%j)
        tpose_path = os.path.join(outf,"tposesk%d.obj"%j)
        tpose_pcd = os.path.join(outf,"tposepcd%d.obj"%j)
        tpose_gt = os.path.join(outf,"tposepcd%d.obj"%j)
        makepath(tpose_path,isfile=True)
        with open(tpose_path, "w+") as fp:
            
            for vertex in tposes[0]:
                fp.write('v %.6f %.6f %.6f %.02f %.02f %.02f\n' %
                            tuple([*vertex, *[0.8,0.3,0.0]]))
        
        makepath(tpose_pcd,isfile=True)         
        with open(tpose_pcd, "w+") as fp:
                    
            for vertex in tpcd[0]:
            
                fp.write('v %.6f %.6f %.6f %.02f %.02f %.02f\n' %
                            tuple([*vertex, *[0.8,0.3,0.0]]))

        for i,frame in enumerate(final_output):
            frame_file = os.path.join(seq_path,"frame/frame%d.obj"%i)
            makepath(frame_file,isfile=True)
            with open(frame_file, "w+") as fp:
            
                for vertex in frame:
                
                    fp.write('v %.6f %.6f %.6f %.02f %.02f %.02f\n' %
                                tuple([*vertex, *[0.8,0.3,0.0]]))
        
        for i,frame in enumerate(final_output_gt):
            frame_file = os.path.join(seq_path,"frame_gt/frame_gt%d.obj"%i)
            makepath(frame_file,isfile=True)
            with open(frame_file, "w+") as fp:
            
                for vertex in frame:
                
                    fp.write('v %.6f %.6f %.6f %.02f %.02f %.02f\n' %
                                tuple([*vertex, *[0.8,0.3,0.0]]))
                    
        for i,frame in enumerate(skeletons_nt[0]+translation[0]):
            frame_file = os.path.join(seq_path,"sk_gt/skeleton%d.obj"%i)
            makepath(frame_file,isfile=True)
            with open(frame_file, "w+") as fp:
            
                for vertex in frame:
                
                    fp.write('v %.6f %.6f %.6f %.02f %.02f %.02f\n' %
                                tuple([*vertex, *[0.8,0.3,0.0]]))
                    
        for i,frame in enumerate(pos_retar[0]+trans_retar[0]):
            frame_file = os.path.join(seq_path,"sk_ret/skeleton_retar%d.obj"%i)
            makepath(frame_file,isfile=True)
            with open(frame_file, "w+") as fp:
            
                for vertex in frame:
                
                    fp.write('v %.6f %.6f %.6f %.02f %.02f %.02f\n' %
                                tuple([*vertex, *[0.8,0.3,0.0]]))


 

if __name__ == "__main__":   
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", type=str,
                        help="Configuration file for the skeleton rnn module, defaults to config_part2.json", default="config_part2.json")
    
    args = parser.parse_args()
    main(args)
    
